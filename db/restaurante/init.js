db.createCollection('restaurantes');
db.restaurantes.insertMany([
    { "id": 1, "nombre": "Restaurante 1" },
    { "id": 2, "nombre": "Restaurante 2" }
]);
db.createCollection('platos');
db.platos.insertMany([
    { "restaurante": 1, "nombre": "Plato 1", "imagen": "https://lorempixel.com/400/300/food/1" },
    { "restaurante": 1, "nombre": "Plato 2", "imagen": "https://lorempixel.com/400/300/food/2" },
    { "restaurante": 2, "nombre": "Plato 1", "imagen": "https://lorempixel.com/400/300/food/3" },
    { "restaurante": 2, "nombre": "Plato 2", "imagen": "https://lorempixel.com/400/300/food/4" }
]);
