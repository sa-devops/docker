# Demostración DevOps para Software Avanzado

## Descripción

El proyecto a continuación consiste en un ambiente Docker (vía docker-compose) capaz de crear los escenarios para los proyectos [Cliente](https://gitlab.com/sa-devops/microservicios/cliente) y [Restaurante](https://gitlab.com/sa-devops/microservicios/restaurante) que componen el ambiente SOA.

## Ejecución

### Paso 1: Clonar el archivo `.env.dist` en `.env` e inicializar las variables de entorno según donde van a ejecutarse los servicios

- **CLIENTE_DB_DATABASE** nombre de la base de datos del microservicio de cliente
- **CLIENTE_DB_PORT** puerto publicado de la base de datos del microservicio de cliente
- **RESTAURANTE_DB_DATABASE** nombre de la base de datos del microservicio de restaurante
- **RESTAURANTE_DB_PORT** puerto publicado de la base de datos del microservicio de restaurante

### Paso 2: Ejecutar el comando

```shell
docker-compose up -d
```
